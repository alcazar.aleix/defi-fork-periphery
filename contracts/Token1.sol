pragma solidity 0.6.6;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Token1 is ERC20 {
constructor () public ERC20("Token1", "T1"){
    _mint(msg.sender,10000 * 10 ** 18);
    }
}
