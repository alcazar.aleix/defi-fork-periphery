const Router = artifacts.require("UniswapV2Router02.sol");
const WETH = artifacts.require("WETH.sol");
const Token1 = artifacts.require("Token1.sol");
const Token2 = artifacts.require("Token2.sol");
module.exports = async function (deployer, network, addresses) {
  let weth;
  const FACTORY_ADDRESS = '0x98d1A3Cce7680dD0bCaE98894E0823ebE928d291';//address del factory previament deployed
  // if(network == 'mainnet'){
    //  bnb = await WETH.at('0xae13d989dac2f0debff460ac112a837c89baa7cd');
  // }else{
    await deployer.deploy(WETH);
    weth = await WETH.deployed();
    await deployer.deploy(Token1);
    await deployer.deploy(Token2);
    const t1 = Token1.deployed();
    const t2 = Token2.deployed();
    console.log("Token 1 Addr: " + t1.address,"Token 2 Addr: " + t2.address );
  // }
  await deployer.deploy(Router, FACTORY_ADDRESS,weth.address);
  // await deployer.deploy(Router);
};
